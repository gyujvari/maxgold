import React from "react";
import Button from "./button";
import "./herosection.css";
import "../App.css";

function Herosection() {
  return (
    <div className="hero-container">
      <video src="/videos/video-2.mp4" autoPlay loop muted />
      <h1>Advanture awaits</h1>
      <p>What are you waitoing for</p>
      <div className="her-btns">
        <Button
          className="btns"
          buttonStyle="btn--outline"
          buttonSize="btn--large"
        >
          Ce vrei ma?
        </Button>
        <Button
          className="btns"
          buttonStyle="btn--primary"
          buttonSize="btn--large"
        >
          Ce vrei ma?
        </Button>
      </div>
    </div>
  );
}

export default Herosection;
