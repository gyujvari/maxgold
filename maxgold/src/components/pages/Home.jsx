import "../../App.css";
import HeroSection from "../herosection";

import React from "react";

function Home() {
  return (
    <>
      <HeroSection />
    </>
  );
}

export default Home;
